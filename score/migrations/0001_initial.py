# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2019-11-03 19:16
from __future__ import unicode_literals

from django.db import migrations, models
import multiselectfield.db.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ImdbScore',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('popularity99', models.FloatField(blank=True)),
                ('director', models.CharField(max_length=500)),
                ('genres', multiselectfield.db.fields.MultiSelectField(choices=[('Adventure', 'Adventure'), ('Family', 'Family'), ('Fantasy', 'Fantasy'), ('Musical', 'Musical'), ('Sci-Fi', 'Sci-Fi'), ('Drama', 'Drama'), ('War', 'War'), ('Romance', 'Romance'), ('Comedy', 'Comedy'), ('Thriller', 'Thriller'), ('Crime', 'Crime'), ('Horror', 'Horror'), ('History', 'History'), ('Family', 'Family'), ('Animation', 'Animation'), ('Short', 'Short'), ('Western', 'Western'), ('Action', 'Action'), ('Biography', 'Biography')], max_length=142)),
                ('imdb_score', models.FloatField(blank=True)),
                ('name', models.CharField(max_length=500, unique=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ['-created'],
            },
        ),
    ]
